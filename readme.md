### Noms : 
        * Barottin Camilo
        * Borg Theo
        * Dufour Matthieu
        * Spacher Loïc
### 1 Recuperer : 
        * sudo docker pull camilobar/mysql
### 2 Cloner le repository :
        * git clone ...
        * cd ActiveRecord
### 3 Lancer : 
        * sudo docker run -d --name orm -p 4321:80 -v $PWD/html:/var/www/html/ camilobar/mysql
### 4 Lancer mysql :
        * sudo docker exec -it orm /bin/bash
        * service mysql start
### 5 Acceder : 
        * http://localhost:4321
### 6 Consulter les fichiers php :
        * cd html