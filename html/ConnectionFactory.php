<?php
class ConnectionFactory{
    
    public static $_connexion;
    
    public static function makeConnection($conf){

        $host=$conf['host'];
        $dbname=$conf['dbname'];
        $charset=$conf['charset'];
        self::$_connexion = new PDO("mysql:host=$host;dbname=$dbname;charset=$charset",$conf['username'],$conf['password']);
        self::$_connexion->setAttribute(PDO::ATTR_PERSISTENT, true);
        self::$_connexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
        self::$_connexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
        self::$_connexion->setAttribute(PDO::ATTR_STRINGIFY_FETCHES,false);
     
        return self::$_connexion;
    }

    public static function getConnection(){
        return self::$_connexion;
    }
}