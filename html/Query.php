<?php
include 'ConnectionFactory.php';

class Query
{
    protected static $_conf;
    protected $_table;
    protected $_fields;
    protected $_condition;
    protected $_request;
    protected static $_instance;
    public function __contruct()
    {
    }

    public static function setConnection($conf){
        self::$_conf = $conf;
    }
   
    public static function table($table)
    {
        self::$_instance = new Query();
        self::$_instance->_table = $table;
        return self::$_instance;
    }

    public function where($field, $operator, $param)
    {
        if (isset($this->_condition)) {
            $this->_condition .= " AND $field $operator '$param'";
        } else {
            $this->_condition = "$field $operator '$param'";
        }
        return $this;
    }

    public function select($fields)
    {      
        $this->_fields = $fields;
        ConnectionFactory::makeConnection(self::$_conf);     
        $res = ConnectionFactory::getConnection()->prepare($this->build());
        $res->execute();
        $r = $res->fetchAll();
        return $r;
    }

    public function build()
    {
        $this->_request = "SELECT $this->_fields FROM $this->_table WHERE $this->_condition;";
        return $this->_request;
    }

    public function delete()
    {
        $this->_request = "DELETE FROM $this->_table WHERE $this->_condition;";
        ConnectionFactory::makeConnection(self::$_conf);
        $res = ConnectionFactory::getConnection()->prepare($this->_request);
        $r=$res->execute();        
        return $r;
    }

    public function insert($params)
    {        
        $values = '';
        for ($i = 0; $i < count($params); $i++) {
            if ($i == count($params) - 1) {
                $values .= "'$params[$i]'";
            } else {
                $values .= "'$params[$i]' ,";
            }
        }
    
        $this->_request = "INSERT INTO $this->_table VALUES ($values) ;";
        ConnectionFactory::makeConnection(self::$_conf);
        $res = ConnectionFactory::getConnection()->prepare($this->_request);
        $res->execute();

        return ConnectionFactory::getConnection()->lastInsertId();
    }
}
